﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Aviasales
{
    public class AviasalesAPIConnection
    {
        public HttpStatusCode StatusCode { get; set; }

        public ListOfCalendarPreload SetUrlForCalendarPreload(string origin, string destination, string departDate)
        {
            string setUrl = $"http://min-prices.aviasales.ru/calendar_preload?origin={ origin }&destination={ destination }&depart_date={ departDate }";
            string response = GetQuerry(setUrl);

            ListOfCalendarPreload content = JsonConvert.DeserializeObject<ListOfCalendarPreload>(response);

            return content;
        }

        public ListOfSupportedDirections SetUrlForSupportedDirections(string origin, string oneWay)
        {
            string setUrl = $"http://map.aviasales.ru/supported_directions.json?origin_iata={ origin }&one_way={oneWay}&locale=ru";
            string response = GetQuerry(setUrl);

            ListOfSupportedDirections content = JsonConvert.DeserializeObject<ListOfSupportedDirections>(response);

            return content;
        }

        public string GetQuerry(string url)
        {
            HttpClient httpClient = new HttpClient();
            Task<HttpResponseMessage> httpResponse = httpClient.GetAsync(url);
            HttpResponseMessage httpResponseMessage = httpResponse.Result;
            StatusCode = httpResponseMessage.StatusCode;
            HttpContent content = httpResponseMessage.Content;
            Task<string> responseData = content.ReadAsStringAsync();
            string data = responseData.Result;
            httpClient.Dispose();
            return data;
        }
    }
}
