﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aviasales
{
    public class Errors
    {
        public string Amadeus { get; set; }
    }

    public class CurrentDepartDatePrice
    {
        public decimal value { get; set; }
        public int trip_class { get; set; }
        public bool show_to_affiliates { get; set; }
        public string return_date { get; set; }
        public string origin { get; set; }
        public int number_of_changes { get; set; }
        public string gate { get; set; }
        public DateTime found_at { get; set; }
        public int distance { get; set; }
        public string destination { get; set; }
        public string depart_date { get; set; }
        public bool actual { get; set; }
    }

    public class BestPrice
    {
        public double value { get; set; }
        public int trip_class { get; set; }
        public bool show_to_affiliates { get; set; }
        public string return_date { get; set; }
        public string origin { get; set; }
        public int number_of_changes { get; set; }
        public string gate { get; set; }
        public DateTime found_at { get; set; }
        public int distance { get; set; }
        public string destination { get; set; }
        public string depart_date { get; set; }
        public bool actual { get; set; }
    }

    public class ListOfCalendarPreload
    {
        public Errors errors { get; set; }
        public List<CurrentDepartDatePrice> current_depart_date_prices { get; set; }
        public List<BestPrice> best_prices { get; set; }
    }
}
