﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aviasales
{
    public class Origin
    {
        public string iata { get; set; }
        public string name { get; set; }
        public string country { get; set; }
        public List<double> coordinates { get; set; }
    }

    public class Weather
    {
        public object weathertype { get; set; }
        public object temp_min { get; set; }
        public object temp_max { get; set; }
    }

    public class Direction
    {
        public bool direct { get; set; }
        public string iata { get; set; }
        public string name { get; set; }
        public string country { get; set; }
        public string country_name { get; set; }
        public List<double?> coordinates { get; set; }
        public int weight { get; set; }
        public Weather weather { get; set; }
    }

    public class ListOfSupportedDirections
    {
        public Origin origin { get; set; }
        public List<Direction> directions { get; set; }
    }
}
