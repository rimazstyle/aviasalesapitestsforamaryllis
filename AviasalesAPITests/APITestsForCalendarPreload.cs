using NUnit.Framework;
using Aviasales;
using System.Net;
using System;

namespace AviasalesAPITests
{
    public class APITestsForCalendarPreload
    {
        string origin = "MOW";
        string destination = "NYC";
        string departureDate = $"{DateTime.Now.Year}-{DateTime.Now.Month}-{DateTime.Now.Day}";
        string wrongDepartureDate = "2020-13-32";
        string wrongOrigin = "123";
        string wrongDestination = "123";

        AviasalesAPIConnection client;

        [SetUp]
        public void Setup()
        {
            client = new AviasalesAPIConnection();
        }

        [Test]
        public void CorrectUrlRequest()
        {
            ListOfCalendarPreload data = client.SetUrlForCalendarPreload(origin, destination, departureDate);

            Assert.Multiple(() => 
            {
                Assert.That(client.StatusCode, Is.EqualTo(HttpStatusCode.OK), "Http statis code is not equal to OK status");
                Assert.That(data.errors.Amadeus, Is.Null, "Errors is not null");
            }                   
            );
        }
        [Test]
        public void OriginDataIsNotCorrect()
        {
            ListOfCalendarPreload data = client.SetUrlForCalendarPreload(wrongOrigin,destination,departureDate);

            Assert.That(data.current_depart_date_prices, Is.Null, "Current departure date is not null!");
        }

        [Test]
        public void DestinationDataIsNotCorrect()
        {
            ListOfCalendarPreload data = client.SetUrlForCalendarPreload(origin, wrongDestination, departureDate);

            Assert.That(data.current_depart_date_prices, Is.Null, "Current departure date is not null!");
        }

        [Test]
        public void DepartureDateIsNotCorrect()
        {
            ListOfCalendarPreload data = client.SetUrlForCalendarPreload(origin, destination, wrongDepartureDate);

            Assert.That(data.best_prices, Is.Null, "Best prices is not null" );
        }

        [Test]
        public void CurrentDepartureDatePriceValueIsNotEqualsToNegative()
        {
            ListOfCalendarPreload data = client.SetUrlForCalendarPreload(origin,destination,departureDate);

            Assert.Multiple(() =>
            {
                foreach (var item in data.current_depart_date_prices)
                {
                    Assert.GreaterOrEqual(item.value, 1, "Value is less than 1");
                }
            });
        }

        [Test]
        public void CurrentDepartureOriginIsEqualToRequestOrigin()
        {
            ListOfCalendarPreload data = client.SetUrlForCalendarPreload(origin, destination, departureDate);

            Assert.Multiple(()=> 
            {
                foreach (var item in data.current_depart_date_prices)
                {
                    Assert.AreEqual(item.origin, origin, "Request origin is not equal to respond origin");
                }
            });
        }

        [Test]
        public void CurrentDepartureDatePriceNumberOfChangesIsNotNegative()
        {
            ListOfCalendarPreload data = client.SetUrlForCalendarPreload(origin,destination, departureDate);

            Assert.Multiple(() =>
            {
                foreach (var item in data.current_depart_date_prices)
                {
                    Assert.GreaterOrEqual(item.number_of_changes, 0, "Number of changes is less than 0 ");
                }
            });
        }

        [Test]
        public void CurrentDepartureTripClassIsNotNegative()
        {
            ListOfCalendarPreload data = client.SetUrlForCalendarPreload(origin, destination, departureDate);

            Assert.Multiple(()=> 
            {
                foreach (var item in data.current_depart_date_prices)
                {
                    Assert.GreaterOrEqual(item.trip_class, 0, "The trip class is Negative");
                }
            });
        }

        [Test]
        public void CurrentDepartureDestinationIsEqualToRequestedDestination()
        {
            ListOfCalendarPreload data = client.SetUrlForCalendarPreload(origin, destination, departureDate);

            Assert.Multiple(()=> 
            {
                foreach (var item in data.current_depart_date_prices)
                {
                    Assert.AreEqual(item.destination, destination, "Requested destination is not equals to responded destination");
                }
            });
        }

        [Test]
        public void BestPricesValueIsNotNegative()
        {
            ListOfCalendarPreload data = client.SetUrlForCalendarPreload(origin, destination, departureDate);

            Assert.Multiple(() =>
            {
                foreach (var item in data.best_prices)
                {
                    Assert.GreaterOrEqual(item.value, 1, "Value is less than 1");
                }
            });
        }

        [Test]
        public void BestPricesNumberOfChangesIsNotNegative()
        {
            ListOfCalendarPreload data = client.SetUrlForCalendarPreload(origin,destination,departureDate);

            Assert.Multiple(() => 
            {
                foreach (var item in data.best_prices)
                {
                    Assert.GreaterOrEqual(item.number_of_changes, 0, "Number of changes is less than 0");
                }
            });
        }
        
        [Test]
        public void BestPricesTripClassItNotNegative()
        {
            ListOfCalendarPreload data = client.SetUrlForCalendarPreload(origin,destination,departureDate);

            Assert.Multiple(() => 
            {
                foreach (var item in data.best_prices)
                {
                    Assert.GreaterOrEqual(item.trip_class, 0, "Trip class is negative");
                }
            });
        }

        [Test]
        public void BestPricesOriginIsEqualToRequestOrigin()
        {
            ListOfCalendarPreload data = client.SetUrlForCalendarPreload(origin,destination,departureDate);

            Assert.Multiple(()=> 
            {
                foreach (var item in data.best_prices)
                {
                    Assert.AreEqual(item.origin, origin, "Request origin is not equal to respond origin");
                }
            });
        }
    }
}