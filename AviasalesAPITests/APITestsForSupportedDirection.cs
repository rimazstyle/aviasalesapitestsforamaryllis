﻿using NUnit.Framework;
using Aviasales;
using System.Net;


namespace AviasalesAPITests
{
    public class APITestsForSupportedDirection
    {
        string origin = "MOW";
        string wrongOrigin = "123";
        string oneWayTrue = "True";
        string oneWayFalse = "False"; 

        AviasalesAPIConnection client;

        [SetUp]
        public void Setup()
        {
            client = new AviasalesAPIConnection();
        }

        [Test]
        public void CorrectUrlRequest()
        {
            ListOfSupportedDirections data = client.SetUrlForSupportedDirections(origin, oneWayTrue);

            Assert.That(client.StatusCode, Is.EqualTo(HttpStatusCode.OK), "Http Status code is not equal to OK status");
        }

        [Test]
        public void CorrectUrlResponse()
        {
            ListOfSupportedDirections data = client.SetUrlForSupportedDirections(origin, oneWayTrue);

            Assert.That(data.origin.iata, Is.EqualTo(origin), "Origin data is not equal");
        }

        [Test]
        public void OriginDataIsNotCorrect()
        {
            ListOfSupportedDirections data = client.SetUrlForSupportedDirections(wrongOrigin, oneWayFalse);

            Assert.Multiple(() =>
            {
                Assert.That(data.directions, Is.Null, "Directions is not null");
                Assert.That(data.origin, Is.Null, "Origin is not null");
            });
        }
    }
}   
